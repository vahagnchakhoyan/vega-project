package com.example.vegaproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.example.vegaproject.viewmodels.UserPageActivityViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class UserPageActivity extends AppCompatActivity {
    private UserPageActivityViewModel userPageActivityViewModel;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_page);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        userPageActivityViewModel = new ViewModelProvider(this, new ViewModelProviderFactory()).get(UserPageActivityViewModel.class);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        sharedPreferences = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!sharedPreferences.getBoolean(getResources().getString(R.string.sp_is_logged_in), false)) {
            Intent intent = new Intent(this, LogInActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void settingsClicked(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
