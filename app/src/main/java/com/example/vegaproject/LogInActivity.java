package com.example.vegaproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LogInActivity extends AppCompatActivity {
    private String fakeLogin = "";
    private String fakePassword = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
    }

    public void logInClicked(View view) {
        String logIn = ((EditText) findViewById(R.id.logInField)).getText().toString();
        String password = ((EditText) findViewById(R.id.passwordField)).getText().toString();

        //ToDo
        if(logIn.equals(fakeLogin) && password.equals(fakePassword)) {
            logInSuccess();
        } else {
            Toast.makeText(this, "Fail!", Toast.LENGTH_SHORT).show();
        }
    }

    private void logInSuccess() {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE).edit();
        sharedPreferencesEditor.putBoolean(getResources().getString(R.string.sp_is_logged_in), true).apply();

        Intent intent = new Intent(this, UserPageActivity.class);
        startActivity(intent);
        finish();
    }
}
