package com.example.vegaproject.viewmodels;

import androidx.lifecycle.ViewModel;

public class SplashScreenViewModel extends ViewModel {
    private float logoTransparency = 0;
    private int afterAnimationPassed = 0;

    public int getAfterAnimationPassed() {
        return afterAnimationPassed;
    }

    public void setAfterAnimationPassed(int afterAnimationPassed) {
        this.afterAnimationPassed = afterAnimationPassed;
    }

    public float getLogoTransparency() {
        return logoTransparency;
    }

    public void setLogoTransparency(float logoTransparency) {
        this.logoTransparency = logoTransparency;
    }
}
