package com.example.vegaproject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.vegaproject.viewmodels.SplashScreenViewModel;

public class SplashScreen extends AppCompatActivity {
    private SplashScreenViewModel splashScreenViewModel;

    private ImageView logoImageView;

    private final int updateDelay = 50;
    private final int animationDuration = 1500;
    private final int afterAnimationInterval = 500;

    //to increase alpha of logo after delay
    private Handler handler;

    private float logoTransparency;
    private int afterAnimationPassed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        logoImageView = findViewById(R.id.logoView);

        //getting live data
        splashScreenViewModel = new ViewModelProvider(this, new ViewModelProviderFactory()).get(SplashScreenViewModel.class);

        logoTransparency = splashScreenViewModel.getLogoTransparency();
        logoImageView.setAlpha(logoTransparency);

        afterAnimationPassed = splashScreenViewModel.getAfterAnimationPassed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(logoTransparency < 1) {
            increaseTransparency();
        } else if(afterAnimationPassed < afterAnimationInterval) {
            increaseAfterAnimationDelay();
        } else {
            decideInitialActivity();
        }
    }

    private void decideInitialActivity() {
        boolean isLoggedIn = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE).getBoolean(getResources().getString(R.string.sp_is_logged_in), false);

        if(isLoggedIn) goTo(UserPageActivity.class);
        else goTo(LogInActivity.class);
    }

    private void goTo(Class<? extends AppCompatActivity> activityClass) {
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
        finish();
    }

    private void increaseTransparency() {
        //increasing transparency coefficient to apply after delay
        if(logoTransparency >= 1) {
            increaseAfterAnimationDelay();
            return;
        }

        logoTransparency += (updateDelay + 0.0) / animationDuration;
        handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                logoImageView.setAlpha(logoTransparency);
                increaseTransparency();
            }
        }, updateDelay);
    }

    private void increaseAfterAnimationDelay() {
        if(afterAnimationPassed >= afterAnimationInterval) {
            decideInitialActivity();
            return;
        }

        afterAnimationPassed += updateDelay;
        handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                increaseAfterAnimationDelay();
            }
        }, updateDelay);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //if there is handler to increase transparency, remove it and save state into live data
        if(handler != null) { handler.removeCallbacksAndMessages(null);}
        splashScreenViewModel.setLogoTransparency(logoTransparency);
        splashScreenViewModel.setAfterAnimationPassed(afterAnimationPassed);
    }
}
