package com.example.vegaproject.interactiveChat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.vegaproject.R;

public class InteractiveChatFragment extends Fragment {

    private InteractiveChatViewModel interactiveChatViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_interactive_chat, container, false);

        ((TextView) root.findViewById(R.id.navigationTitle)).setText(R.string.title_interactive_chat);
        return root;
    }
}