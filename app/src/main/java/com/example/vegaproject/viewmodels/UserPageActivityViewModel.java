package com.example.vegaproject.viewmodels;

import androidx.lifecycle.ViewModel;

public class UserPageActivityViewModel extends ViewModel {
    private boolean isLoggedIn = true;

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }
}
