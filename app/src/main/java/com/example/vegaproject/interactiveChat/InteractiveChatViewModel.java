package com.example.vegaproject.interactiveChat;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class InteractiveChatViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public InteractiveChatViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is interactive chat fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}