package com.example.vegaproject;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void logOutClicked(View view) {
        getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE).edit().putBoolean(getResources().getString(R.string.sp_is_logged_in), false).apply();
        finish();
    }

    public void backClicked(View view) {
        finish();
    }

}
